#!/bin/bash
sed -i -e "s/\/bin\/python3/\/usr\/bin\/python3/" voicer.py
sed -i -e "s/ca-bundle/ca-certificates/g" voicer.py
sed -i -e "s/PROTOCOL_TLS/PROTOCOL_TLSv1_2/g" voicer.py
