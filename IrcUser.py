#import IrcChan

class UserIDCard:
    nick = ""
    realuser = ""
    domain = ""
#    onChan = []
    modes = {
#        o = []      #operator of chan
        'i' : False,   #invisible
        'g' : False,   #? +g     - "caller id" mode only allow accept clients to message you
        'w' : False,   #? +w     - Can see oper wallops.
        'D' : False,   # +D     - Deaf - ignores all channel messages.
        'Q' : False,   # +Q     - Prevents you from being affected by channel forwarding.
        'R' : False,   # +R     - Prevents non accept unidentified users from messaging you.
        'Z' : False,   # +Z     - Is connected via SSL (cannot be set or unset).
        }
    
    def __init__(self, fullname):
        #:Voicer!~Voicer@2a02:8071:9289:5900:4a51:b7ff:fe84:99e6 JOIN #jibzot
        #print("\t\tFonction création User : "+fullname)
        if fullname.count("!") > 0:
            #print("\t\tDetecte full")
            #on s'assure d'avoir que la portion nick!realuser@domain
            fullname = fullname.strip(" ").strip(":").split(" ")[0]
            #debNick = 0
            finNick = fullname.find("!")
            debReal = finNick+2
            finReal = fullname.find("@")
            debDoma = finReal+1
            #finDoma = len(fullname)-1
            #self.nick = fullname[debNick:finNick]
            self.nick = fullname[:finNick]
            self.realuser = fullname[debReal:finReal]
            #domainToCheck = fullname[debDoma:finDoma]
            domainToCheck = fullname[debDoma:]
            #need to check domaine ? Or its format ? It's IP ? in order to provide autovoice for VIP ?
            self.domain = domainToCheck
        else:
            #print("\t\tDetecte nick seul")
            self.nick = fullname
        #print("\t\tNick retenu : "+self.nick+"\n\t\tReal retenu : "+self.realuser+"\n\t\tDoma retenu : "+self.domain)
    
    def getNick(self):
        return self.nick
    
    def getDomain(self):
        return self.domain
    
    def getRealUser(self):
        return self.realuser
    
    def getFullname(self):
        return self.nick+"!"+realuser+"@"+domain
    
