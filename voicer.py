#!/bin/python3
#BUG : parfois /bin/python3 n'est pas reconnu. Il faut le changer par /usr/bin/python3 /
# bot voicer d'IRC après un temps.

#socket pour la connexion internet
#ssl pour le chiffrage de la connexion
#threading pour gérer les messages reçu et les messages envoyer en même temps
#queue pour plusieurs messages en sortie
import socket, ssl, threading, queue
#IrcCode un fichier avec les code de réponse d'Irc
import IrcCode
from IrcCode import IrcCodeDic
from IrcCode import CodeFromMeaning
from IrcChan import Chan
from IrcUser import UserIDCard
#from IrcCode import ByCol

HOST = 'irc.freenode.org'
PORT = 6697
EOL = "\n\r"
class state(object):
    """Espace de noms pour les variables et fonctions <pseudo-globales>"""
    knowServ = False
    ServName = ""
    BotUser = 'Voicer'
    BotNick = 'Voicer[dev]'
    SendQueue = queue.Queue()
    HideLine = 0;
    dicoChan = {
        }

def voiceUser(user, room):
    if state.dicoChan[room].isUser(user) == True :
        user = user.split("!")[0]
        messageVoiceant = "MODE "+room+" +v "+user
        state.SendQueue.put(messageVoiceant)

def fct_ping(to):
    retMsg = "PONG :"+to
    state.SendQueue.put(retMsg)

def fct_login():
    usermsg = "USER "+ state.BotUser +" "+ state.BotUser +" "+ state.BotUser +" "+ state.BotUser
    state.SendQueue.put(usermsg)
    usermsg = "NICK "+ state.BotNick
    state.SendQueue.put(usermsg)

def fct_privatmsg(message):
    #:Jybz!~jibz@2a02:8071:9289:5900:4a51:b7ff:fe84:99e6 PRIVMSG #carabistouille :hrt
    #:Jybz!~jibz@2a02:8071:9289:5900:4a51:b7ff:fe84:99e6 PRIVMSG Voicer[dev] :hrt
    endHeader = message.find(" :")+1
    header = message[:endHeader]
    content = message[endHeader+1:]
    
    if not header.split(" ")[2] == state.BotNick :
        destination = header.strip(":").strip(" ").split(" ")[2]
    else :
        destination = header.strip(":").strip(" ").split(" ")[0].split("!")[0]
        
    parsed=content.split(" ")
    gotJoin = False
    gotChan = False
    retMsg = ""
    for part in parsed:
        if part.count("#") >0 :
            gotJoin = True
            chantojoin = part
        elif part.count("join") >0 :
            gotChan = False
        elif part.count("help") >0 :
            print("Here continue to dev")
            
    if gotJoin == True and gotChan == True :
#            fct_EnterRoom(chantojoin)
        print("Here continue to dev")

def fct_Chanmsg(message):
    #:Jybz!~jibz@ip-37-201-6-240.hsi13.unitymediagroup.de PRIVMSG #carabistouille :message
    parsed=message.split(" ")
    retMsg = ""
    for part in parsed:
        if part.count("#") == 1 and part.find("#") == 0:
#            print("SALON DETECTÉ !!!")
            #retMsg = fct_EnterRoom(part)
            break
    return retMsg

def fct_EnterRoom(Chan):
    retMsg = "JOIN "+Chan
    state.SendQueue.put(retMsg)
    #return retMsg

def fct_none(whatever):
    return ""

def fct_ChanUserMGMT(arg):
    #command, user, room
    return ""


def messageParser(msgin):
    ContVal = 1
    try:
        #message = unicode(msgin, 'utf-8')
        message = msgin.decode("Utf8",'surrogateescape')
    except UnicodeDecodeError:
        print("\t\tERROR : Invalid input")
        message = msgin
        ContVal = 0

    
    if ContVal ==1 :
        #print("\tIn Parser !")
        retVal = ""
        #retTab = ['noact','noact'] #utile pour l'optimisation, mais chaque if doit avoir une instruction au minimum...
        endHeader = message.find(" :")+1
        if endHeader == 0:
            header = message
            content = ""
        else:
            header = message[:endHeader]
            if ContVal ==1:
                content = message[endHeader+1:]
        #print("\t"+header+"\t"+content)
            
        
        #PING
        if header.count("PING") > 0 : #PING :wolfe.freenode.net
            fct_ping(state.ServName)
        
        #RPL_BOUNCE
        #elif header.count(":"+state.ServName+" "+CodeFromMeaning["RPL_BOUNCE"][0]) > 0:
        #    retTab = ['noact','noact']
        
        #PRIVMSG
        elif header.count("PRIVMSG") > 0: #:Jybz!~jibz@2a02:8071:9289:5900:4a51:b7ff:fe84:99e6 PRIVMSG Voicer :VERSION
            #print("Found message")
            if header.count("#") >0:
                #print("Message in chan !")
                #it is from a chan
                for part in header.split(" "):
                    if part.count("#"):
                        #print("Chan is "+part)
                        retVal = state.dicoChan[part].incomeMsg(message)
                        if not retVal == "":
                            message = "PRIVMSG "+part+" :"+retVal
                            state.SendQueue.put(message)
                        #else:
                            #print("no return message")
            elif header.count(state.BotNick) >0 :
                #private message direct to bot.
                retVal = fct_privatmsg(message)

                    

        #INVITE
        elif header.count("INVITE") > 0:
            if content.count("#") >0:
                #state.dicoChan[content] = Chan(state.BotNick, content)
                #print("\t"+content)
                fct_EnterRoom(content)

        #JOIN
        elif header.count("JOIN"):
            #print("\tJoin response found")
            if header.count(state.BotNick)>0:
                #The bot just came in the Room
                #print("\tThe Bot enter in the room.")
                for part in header.strip(" ").split(" "):
                    if part.count("#") >0:
                        #print("\tCHECK : the room is "+part)
                        state.dicoChan[part] = Chan(state.BotNick, part)
                        break
            else :
                headerparts = header.strip(" ").split(" ")
                fulluser = headerparts[0][1:]
                roomname = headerparts[2]
                state.dicoChan[roomname].addUser(fulluser)
                nick = fulluser.split("!")[0]
                args = [nick,roomname]
                if state.dicoChan[roomname].autoVoice == True :
                    threading.Timer(state.dicoChan[roomname].timeBeforeVoice, voiceUser, args).start()
                    if state.dicoChan[roomname].autoNotice == True :
                        NoticeMsg = "NOTICE "+nick+" :"+state.dicoChan[roomname].getNotice()
                        state.SendQueue.put(NoticeMsg)
                        


        elif header.count(CodeFromMeaning["RPL_NAMREPLY"][0]):
            for part in header.strip(" ").split(" "):
                if part.count("#") >0:
                    #print("\t"+part+" "+content)
                    state.dicoChan[part].UsersAlreadyHere(content)

        elif header.count("NICK")>0 : #:test2!~jibz@2a02:8071:9289:5900:4a51:b7ff:fe84:99e6 NICK :testi
            whichUser = header.strip(" ").split(" ")[0]
            userStart = 1
            userStop = whichUser.find("!")
            UserNick = whichUser[userStart:userStop]
            NewNick = content
            #print("\t\t"+header+"\n\t\t"+content)
            for channel in state.dicoChan.keys() :
                if state.dicoChan[channel].isUser(UserNick) == True :
                    state.dicoChan[channel].renameUser(UserNick, NewNick)
                    
        #QUIT PART KICK
        elif header.count("QUIT")>0 : #:stroibe974!~sebastien@165.169.130.167 QUIT :Quit: Konversation terminated!
            whichUser = header.strip(" ").split(" ")[0]
            userStart = 1
            userStop = whichUser.find("!")
            UserNick = whichUser[userStart:userStop]
            #we have the nick, but on which f* channel ??
            for channel in state.dicoChan.keys() :
                if state.dicoChan[channel].isUser(UserNick) == True :
                    state.dicoChan[channel].remUser(UserNick)
                #for oneUser in channel.Users.keys() :
                #    print(oneUser)
                #    if oneUser.nick == UserNick :
                #        channel.remUser(UserNick)
        
        elif header.count("PART")>0 : #:Jybz!~jibz@ip-37-201-6-240.hsi13.unitymediagroup.de PART #carabistouille :"Konversation terminated!"
            parts = header.strip(":").strip(" ").split(" ")
            if parts[2].count("#")>0:
                channel = parts[2]
                whichUser = parts[0].split("!")[0]
                state.dicoChan[channel].remUser(whichUser)
            #for part in header.strip(":").strip(" ").split(" "):
            #    if part.count("#") >0:
            #        #we found the canal where a user
            #        whichUser = header.strip(" ").split(" ")[0]
            #        userStart = 1
            #        userStop = whichUser.find("!")
            #        whichUser = whichUser[userStart:userStop]
            #        state.dicoChan[part].remUser(whichUser)
            
        elif header.count("KICK")>0 : #:Jybz!~jibz@ip-37-201-6-240.hsi13.unitymediagroup.de KICK #carabistouille Voicer :User terminated!
            parts = header.strip(" ").split(" ")
            for part in parts:
                if part.count("#") >0:
                    #we found the canal where a user have been kicked.
                    whichUser = header.strip(" ").split(" ")
                    whichUser = parts[-1]
                    state.dicoChan[part].remUser(whichUser)
        
        #Début de connexion, enregistrement d'identifiants.
        elif message.count(":"+state.ServName+" NOTICE * :*** No Ident response") > 0: #:hobana.freenode.net NOTICE * :*** No Ident response
            fct_login()

        #NICK USED !!!
        elif message.count(":"+state.ServName+" "+CodeFromMeaning["ERR_NICKNAMEINUSE"][0]) > 0: #:hobana.freenode.net NOTICE * :*** No Ident response
            #fct_renick()
            print("\n\n\tRENAME THE BOT !\n\n")
        
        #RPL_WELCOME Message de bienvenu
        #elif message.count(":"+state.ServName+" "+CodeFromMeaning["RPL_WELCOME"][0]) > 0:
        #    retTab = ['noact','noact']
        
        #RPL_ENDOFMOTD Fin du mot de bienvenu, vérification du Nick
        #elif message.count(":"+state.ServName+" "+CodeFromMeaning["RPL_ENDOFMOTD"][0]) > 0: #:moon.freenode.net 376 Voicer :End of /MOTD command. #update nick
        #    retTab = ['noact','noact']
        #    state.BotNick = message.split(" ")[2]

        #Regarde si nous connaissont déjà le serveur
        elif not state.knowServ:
            #retTab = ['noact','noact']
            state.ServName = message.split(":")[1].split(" ")[0]
            state.knowServ = True
    else :
        print("\t\tError parsing...")
        return 0
     
class ThreadProcess(threading.Thread):
    """objet thread gérant l'émission des messages"""

    def __init__(self, message):
        threading.Thread.__init__(self)
        self.imsg = message

    def run(self):
        messageParser(self.imsg)

class ThreadReception(threading.Thread):
    """objet thread gérant la réception des messages"""

    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn          

    def run(self):
        while 1:
            message_recu = self.connexion.recv(8192)
            if not message_recu :
                break
            split = message_recu.strip(b'\n\r').split(b"\n")
            for part in split:
                part = part.strip(b"\n").strip(b"\r")
                print(part) #+"\n")
                ThreadProcess(part).start()
        self.connexion.close()
        print("Thread Reception exit")

class ThreadEmission(threading.Thread):
    """objet thread gérant l'émission des messages"""

    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn    

    def run(self):
        while 1:
            message_aemettre = state.SendQueue.get(block = True)
            message_emis = message_aemettre+EOL
            if state.HideLine == 0 :
                print("\t"+message_aemettre)
            self.connexion.send(message_emis.encode())
            state.HideLine = 0
            if message_aemettre.find("QUIT") == 0:
                break
        print("Thread Emission exit")

class ThreadManuel(threading.Thread):
    """objet thread gérant l'émission des messages"""

    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while 1:
            message_entre = input()
            if message_entre.count("\\x") >0:
                message_aemettre = message_entre.replace("\\x01", "\x01").replace("\\xe9", "\xe9")
            else:
                message_aemettre = message_entre
            state.SendQueue.put(message_aemettre)
            state.HideLine = 1
            if message_aemettre.find("QUIT") == 0 :
                break
        print("Thread Manuel exit")

#def main():
mySocket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

context = ssl.create_default_context()
#WARNING l'objet ssl peut contenir des variables différentes, comme ssl.PROTOCOL_TLSv1_2 , résolution possible "Venv" environnement virtuel.
context = ssl.SSLContext(ssl.PROTOCOL_TLS)
context.verify_mode = ssl.CERT_REQUIRED
context.check_hostname = True
#WARNING le certificat peut être : /etc/ssl/certs/ca-certificates.crt
context.load_verify_locations("/etc/ssl/certs/ca-bundle.crt")

conn = context.wrap_socket(mySocket, server_hostname=HOST)
try:
    conn.connect((HOST, PORT))
except socket.error:
    print("\tEchec connexion. Quit.")
    quit()

th_R = ThreadReception(conn)
th_M = ThreadManuel()
th_E = ThreadEmission(conn)

th_E.start()
th_M.start()
th_R.start()

th_M.join()
th_E.join()
th_R.join()
quit()

