from IrcUser import UserIDCard
#import IrcUser

class UserRights:
    def __init__(self):
        operator = False
        voice = False

    def changeRight(self, newRight):
        if newRight.count("o")>0:
            if newRight.count("-")>0:
                self.operator = False
            elif newRight.count("+")>0:
                self.operator = True
        elif newRight.count("v")>0:
            if newRight.count("-")>0:
                self.voice = False
            elif newRight.count("+")>0:
                self.voice = True

class ChanUser:
    def __init__(self, nUser, nRights):
        self.IDC = nUser
        self.rights = nRights
        
class Chan:
    #selfnick = ""
    #Name = ""
    Users = {}
    modes = {
        'n' : False,   # - No external messages.  Only channel members may talk in the channel
        't' : False,   # - Ops Topic.  Only opped (+o) users may set the topic.
        's' : False,   # - Secret.  Channel will not be shown in /whois and /list etc.
        'p' : False,   # - Private.  Disables /knock to the channel.
        'm' : False,   # - Moderated.  Only opped/voiced users may talk in channel.
        'i' : False,   # - Invite only.  Users need to be /invite'd or match a +I to join the channel.
        'r' : False,   # - Registered users only.  Only users identified to services may join.
        'c' : False,   # - No color.  All color codes in messages are stripped.
        'g' : False,   # - Free invite.  Everyone may invite users.  Significantly weakens +i control.
        'z' : False,   # - Op moderated.  Messages blocked by +m, +b and +q are instead sent to ops.
        'L' : False,   # - Large ban list.  Increase maximum number of +beIq entries. Only settable by opers.
        'P' : False,   # - Permanent.  Channel does not disappear when empty.  Only settable by opers.
        'F' : False,   # - Free target.  Anyone may set forwards to this (otherwise ops are necessary).
        'Q' : False,   # - Disable forward.  Users cannot be forwarded to the channel (however, new forwards can still be set subject to +F).
        'C' : False,   # - Disable CTCP. All CTCP messages to the channel, except ACTION, are disallowed.
        'f' : False,   # - Forward.  Forwards users who cannot join because of +i, +j, +l or +r. PARAMS: /mode #channel +f #channel2
        'j' : False,   # - Join throttle.  Limits number of joins to the channel per time. PARAMS: /mode #channel +j count:time
        'k' : False,   # - Key.  Requires users to issue /join #channel KEY to join. PARAMS: /mode #channel +k key
        'l' : False,   # - Limit.  Impose a maximum number of LIMIT people in the channel. PARAMS: /mode #channel +l limit
        'v' : False,   # - Voice.  Allows a user to talk in a +m channel.  Noted by +nick. PARAMS: /mode #channel +v nick
        'o' : False,   # - Op.  Allows a user full control over the channel. PARAMS: /mode #channel +o nick
        'b' : False,   # - Ban.  Prevents a user from entering the channel, and from sending or changing nick if they are on it, based on a nick!ident@host match. PARAMS: /mode #channel +b nick!user@host
        'q' : False,   # - Quiet.  Prevents a user from sending to the channel or changing nick, based on a nick!ident@host match. PARAMS: /mode #channel +q nick!user@host
        'e' : False,   # - Exempt.  Allows a user to join a channel and send to it even if they are banned (+b) or quieted (+q), based on a nick!ident@host match. PARAMS: /mode #channel +e nick!user@host
        'I' : False,   # - Invite Exempt.  Allows a user to join a +i channel without an invite, based on a nick!user@host match. PARAMS: /mode #channel +I nick!user@host
    }
    #timeBeforeVoice = 10
    #noticeMessage = ""
    #autoVoice = False
    #autoNotice = True
    #lockedBy = ""

    def __init__(self, robotnick, newChan):
        if newChan.find("#") == -1:
            newChan = "#"+newChan
        self.Name = newChan
        self.timeBeforeVoice = 10
        self.noticeMessage = "You will be allowed to write in the chan " +self.Name+ " in "+str(self.timeBeforeVoice)+" seconds"
        self.autoVoice = False
        self.autoNotice = False
        self.lockedBy = ""
        self.robotnick = robotnick

    def UsersAlreadyHere(self, usersList):
        #:hobana.freenode.net 353 Voicer = #rmll-2018 :Voicer +malade_mental +mmu_man +flo2marsnet +fabricius +rainer +merlin8282 @mhep_ @Alaiks @Jyhem_ @Jyhem_laptop @Jyhem__ @Jybz +Coolgeek @Legaume +marja +linkmauve +Spydemon +Sigyn oumph @voicerobsolet +PulkoMandy +madix @johndescs +tuxayo1 +MatrixTravelerbo +Guest88000 +garfieldairlines +myckeul_ @ced117 @Neelfyn @caswitch +Aime38___ +TsT +robertf +olive` +amj +madonius +Bat`O_ @jeremiew +cpm_screen +dlareg +cemoi +n4mu +bohwaz @harmonie_
        #:hobana.freenode.net 366 Voicer #rmll-2018 :End of /NAMES list.
        #:hobana.freenode.net 353 Voicer @ #carabistouille :Voicer @Jybz
        if usersList.find(":") == 0 and usersList.count("353") >0 :
            usersList = usersList.split(":")[2]
        usersList = usersList.split(" ")
        for oneUser in usersList:
            HisRights = UserRights()
            if oneUser.find("@") == 0:
                HisRights.changeRight("+o")
                oneUser=oneUser[1:]
            if oneUser.find("+") == 0:
                HisRights.changeRight("+v")
                oneUser=oneUser[1:]
            newUser = ChanUser(UserIDCard(oneUser), HisRights)
            self.Users[newUser.IDC.nick] = newUser
    
    def isUser(self, potentialUser):
        return potentialUser in self.Users
    
    def addUser(self, newdUserFullName):
        #:Voicer!~Voicer@2a02:8071:9289:5900:4a51:b7ff:fe84:99e6 JOIN #rmll-2018
        HisRights = UserRights()
        newUser = ChanUser(UserIDCard(newdUserFullName), HisRights)
        self.Users[newUser.IDC.getNick()] = newUser

    def remUser(self, remnick):
        del self.Users[remnick]
        
    def renameUser(self, nick, newnick):
        self.Users[newnick] = self.Users[nick]
        del self.Users[nick]
    
    def newRight(self, sentence):
        #:Jybz!~jibz@ip-37-201-6-240.hsi13.unitymediagroup.de MODE #carabistouille -v Jybz
        if sentence.find(":") == 0 and sentence.count("MODE") >0 :
            splitedSentence = sentence.split(" ")
            self.newRight(splitedSentence[4], splitedSentence[3])
            
    def newRight(self, nick, right):
        #:Jybz!~jibz@ip-37-201-6-240.hsi13.unitymediagroup.de MODE #carabistouille -v Jybz
        self.Users[nick].IDC.rights.changeRight(right)
        
    def setNotice(self, newNoticeSentence, nick):
        retVal = "Noki..."
        if self.lockedBy == "" or self.lockedBy == nick :
            if newNoticeSentence.upper().count("NOTICE") >0:
                self.noticeMessage = newNoticeSentence.split("\"")[1]
                retVal = "Oki doki."
        return retVal
    
    def setTime(self, newTimeSentence, nick):
        retVal = "Noki..."
        if self.lockedBy == "" or self.lockedBy == nick :
            if newTimeSentence.upper().count("TIME") >0:
                for part in newTimeSentence.strip(" ").split(" "):
                    if part.upper().count("SEC") >0:
                        end = part.upper().find("S")
                        newTime = part[:end]
                        if newTime.isdecimal() :
                            self.timeBeforeVoice = int(newTime)
                            retVal = "Oki doki "+str(self.timeBeforeVoice)+"!"
        return retVal
                
    def getTime(self):
        msg = "Autovoicing time set to "+str(self.timeBeforeVoice)+" seconds."
        return msg
        
    def startVoice(self):
        self.autoVoice = True
        retVal = "Go!"
        return retVal
    
    def stopVoice(self, nick):
        retVal = "Noki..."
        if self.lockedBy == "" or self.lockedBy == nick :
            self.autoVoice = False
            retVal = "Stop."
        return retVal
        
    def startNotice(self):
        self.autoNotice = True
        return "Go!"
        
    def stopNotice(self, nick):
        retVal = "Noki..."
        if self.lockedBy == "" or self.lockedBy == nick :
            self.autoNotice = False
            retVal = "Stop."
        return retVal

    def getNotice(self):
        return self.noticeMessage
    
    def lock(self, nick):
        retVal = "Noki..."
        if self.lockedBy == "" or self.lockedBy == nick :
            self.lockedBy = nick
            retVal = nick+" locked me."
        return retVal
    
    def unlock(self, nick):
        retVal = "Noki..."
        if self.lockedBy == "" or self.lockedBy == nick :
            self.lockedBy = ""
            retVal = "Unlocked"
        return retVal
        
    def getLocker(self):
        retVal = "Not locked."
        if not self.lockedBy == "":
            retVal = "Locked by "+self.lockedBy+"."
        return retVal
        
    def incomeMsg(self, message):
        retVal = ""
        contentdeb = message.find(" :")+2
        header = message[:contentdeb].upper()
        content = message[contentdeb:]
        nick = message.strip(":").split("!")[0]
        if header.count("MODE") >0:
        #    print("found Mode")
            self.newRight(message)
        elif header.count("JOIN") >0:
        #    print("found join")
            self.addUser(message)
        elif header.count("QUIT")>0 or header.count("PART")>0:
        #    print("found quit or part")
            end = message.find("!")-1
            self.remUser(message[1:end])
        elif header.count("KICK")>0:
        #    print("found kick")
            self.remUser(message.split(" ")[3])
        elif content.count(self.robotnick)>0 :
            if header.count("PRIVMSG")>0 :
            #    print("found private message")
                if content.upper().count("NOTICE") >0:
                #    print("found notice")
                    if content.upper().count("GET") >0:
                #        print("found get")
                        retVal = self.getNotice()
                    elif content.upper().count("SET") >0:
                #        print("found set")
                        retVal = self.setNotice(content, nick)
                    elif content.upper().count("START") >0:
                #        print("found start")
                        retVal = self.startNotice()
                    elif content.upper().count("STOP") >0:
                #        print("found stop")
                        retVal = self.stopNotice(nick)
                elif content.upper().count("UNLOCK")>0:
                    retVal = self.unlock(nick)
                elif content.upper().count("LOCK")>0:
                #    print("found lock")
                    if content.upper().count("WHO")>0:
                #        print("found who")
                        retVal = self.getLocker()
                    elif content.upper().count("SET")>0:
                        #end = content.find("!")
                        #nick = content[1:end]
                        retVal = self.lock(nick)
                elif content.upper().count("TIME")>0:
                #    print("found time")
                    if content.upper().count("SET") >0:
                #        print("found Set")
                        retVal = self.setTime(content, nick)
                    elif content.upper().count("GET") >0:
                #        print("found get")
                        retVal = self.getTime()
                elif content.upper().count("VOICE")>0:
                #    print("found voice")
                    if message.upper().count("START") >0:
                #        print("found found start")
                        retVal = self.startVoice()
                #        print("\t"+retVal)
                    elif message.upper().count("STOP") >0:
                #        print("found stop")
                        retVal = self.stopVoice(nick)
        elif message.split(" ")[1].count("353") >0:
            self.UsersAlreadyHere(message)
        #print("\t"+retVal)
        return retVal
